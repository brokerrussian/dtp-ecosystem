const $mongo = require('../libs/mongo');

const postsDB = $mongo.collection('posts');
const usersDB = $mongo.collection('users');
const postDraftsDB = $mongo.collection('postDrafts');
const userScriptDB = $mongo.collection('userScript');
const ratesDB = $mongo.collection('rates');
const sessionsDB = $mongo.collection('sessions');
const contentIndexDB = $mongo.collection('contentIndex');

const groupAccountsDB = $mongo.collection('groupAccounts');
const groupAccountsMembersDB = $mongo.collection('groupAccountsMembers');
const trendingFiltersDB = $mongo.collection('trendingFilters');
const groupAccountsInvitesDB = $mongo.collection('groupAccountsInvites');
const authorNamesSandboxDB = $mongo.collection('authorNamesSandbox');
const contentCacheDB = $mongo.collection('contentCache');
const contentBlacklistDB = $mongo.collection('contentBlacklist');

const telegraphTokensDB = $mongo.collection('telegraphTokens');
const postCoversDB = $mongo.collection('postCovers');

const defaultOptions = {
  background: true
};
const uniqueOptions = {
  unique: true
};

const textIndexOptions = {
  default_language: 'none',
  language_override: 'none'
};

const notNULL = {$exists: true};

function reportError (err) {
  console.log(err);
}

async function createIndexes(params) {
  await new Promise((resolve, reject) => {
    function _await () {
      if (typeof usersDB.collection !== 'undefined') {
        resolve();
      } else {
        setTimeout(_await, 500);
      }
    }

    _await();
  });
 
  await postsDB.collection.createIndex({ user_id: -1 }, defaultOptions).catch(reportError);
  await postsDB.collection.createIndex({ account_id: -1 }, defaultOptions).catch(reportError);
  await postsDB.collection.createIndex({ path: -1 }, Object.assign({}, defaultOptions, uniqueOptions, {
    partialFilterExpression: {
      path: notNULL
    }
  })).catch(reportError);
  await postsDB.collection.createIndex({ shortId: -1 }, Object.assign({}, defaultOptions, uniqueOptions, {
    partialFilterExpression: {
      shortId: notNULL
    }
  })).catch(reportError);
  await postsDB.collection.createIndex({ 'preview.views': -1 }, defaultOptions).catch(reportError);
  await postsDB.collection.createIndex({ language: -1 }, defaultOptions).catch(reportError);
  await postsDB.collection.createIndex({ tags_index: 'text' }, Object.assign({}, defaultOptions, textIndexOptions)).catch(reportError);
  await postsDB.collection.createIndex({ createdDate: -1 }, defaultOptions).catch(reportError);
  await postsDB.collection.createIndex({ 'votes.total': -1 }, defaultOptions).catch(reportError);
  await postsDB.collection.createIndex({
    'votes.total': -1,
    'preview.views': -1
  }, defaultOptions).catch(reportError);

  await usersDB.collection.createIndex({ id: -1 }, Object.assign({}, defaultOptions, uniqueOptions, {
    partialFilterExpression: {
      id: notNULL
    }
  })).catch(reportError);
  await usersDB.collection.createIndex({ secretToken: -1 }, Object.assign({}, defaultOptions, uniqueOptions, {
    partialFilterExpression: {
      secretToken: notNULL
    }
  })).catch(reportError);
  await usersDB.collection.createIndex({ openToken: -1 }, Object.assign({}, defaultOptions, uniqueOptions, {
    partialFilterExpression: {
      openToken: notNULL
    }
  })).catch(reportError);
  await usersDB.collection.createIndex({ last_bot_session: -1 }, defaultOptions).catch(reportError);
  await usersDB.collection.createIndex({ last_api_session: -1 }, defaultOptions).catch(reportError);

  await postDraftsDB.collection.createIndex({ user_id: -1 }, defaultOptions).catch(reportError);

  await ratesDB.collection.createIndex({ user_id: -1 }, defaultOptions).catch(reportError);
  await ratesDB.collection.createIndex({ shortId: -1 }, defaultOptions).catch(reportError);

  await sessionsDB.collection.createIndex({ sessionid: -1 }, Object.assign({}, defaultOptions, uniqueOptions, {
    partialFilterExpression: {
      sessionid: notNULL
    }
  })).catch(reportError);

  await contentIndexDB.collection.createIndex({ content: 'text' }, Object.assign({}, defaultOptions, textIndexOptions)).catch(reportError);
  await contentIndexDB.collection.createIndex({ shortId: -1 }, defaultOptions).catch(reportError);
  await contentIndexDB.collection.createIndex({ createdDate: -1 }, defaultOptions).catch(reportError);


  await groupAccountsDB.collection.createIndex({ id: -1 }, Object.assign({}, defaultOptions, uniqueOptions, {
    partialFilterExpression: {
      id: notNULL
    }
  })).catch(reportError);
  await groupAccountsDB.collection.createIndex({ username: -1 }, Object.assign({}, defaultOptions, uniqueOptions, {
    partialFilterExpression: {
      username: notNULL
    }
  })).catch(reportError);
  await groupAccountsDB.collection.createIndex({ createdDate: -1 }, defaultOptions).catch(reportError);
  await groupAccountsMembersDB.collection.createIndex({ user_id: -1 }, defaultOptions).catch(reportError);
  await groupAccountsMembersDB.collection.createIndex({ account_id: -1 }, defaultOptions).catch(reportError); 
  await groupAccountsInvitesDB.collection.createIndex({ id: -1 }, Object.assign({}, defaultOptions, uniqueOptions, {
    partialFilterExpression: {
      id: notNULL
    }
  })).catch(reportError); 
  await groupAccountsInvitesDB.collection.createIndex({ account_id: -1 }, defaultOptions).catch(reportError); 
  await trendingFiltersDB.collection.createIndex({ user_id: -1 }, defaultOptions).catch(reportError); 
  
  await telegraphTokensDB.collection.createIndex({ user_id: -1 }, defaultOptions).catch(reportError); 
  await telegraphTokensDB.collection.createIndex({ account_id: -1 }, defaultOptions).catch(reportError); 
  await telegraphTokensDB.collection.createIndex({ createdDate: -1 }, defaultOptions).catch(reportError); 
  
  await authorNamesSandboxDB.collection.createIndex({ createdDate: -1 }, defaultOptions).catch(reportError); 
  await authorNamesSandboxDB.collection.createIndex({ username: -1 }, Object.assign({}, defaultOptions)).catch(reportError); 
  await authorNamesSandboxDB.collection.createIndex({ user_id: -1 }, defaultOptions).catch(reportError); 
 
  await contentCacheDB.collection.createIndex({ path: -1 }, defaultOptions).catch(reportError); 
  await contentCacheDB.collection.createIndex({ time: -1 }, defaultOptions).catch(reportError); 
  await contentCacheDB.collection.createIndex({ date: -1 }, Object.assign({}, defaultOptions, {
    expireAfterSeconds: 1000
  })).catch(reportError); 
  
  await contentBlacklistDB.collection.createIndex({ type: -1 }, defaultOptions).catch(reportError); 
  await contentBlacklistDB.collection.createIndex({ value: -1 }, defaultOptions).catch(reportError); 
  await contentBlacklistDB.collection.createIndex({ value: -1, type: -1 }, Object.assign({}, defaultOptions, uniqueOptions, {
    partialFilterExpression: {
      value: notNULL,
      type: notNULL
    }
  })).catch(reportError); 
  
  await postCoversDB.collection.createIndex({ contentHash: -1 }, Object.assign({}, defaultOptions, uniqueOptions, {
    partialFilterExpression: {
      contentHash: notNULL
    }
  })).catch(reportError); 
  await postCoversDB.collection.createIndex({ createdDate: -1 }, defaultOptions).catch(reportError); 
}

console.log('Started!');
createIndexes()
  .then(() => {
    console.log('Done');
    process.exit();
  })
  .catch((err) => {
    console.log(err);
    process.exit();
  });