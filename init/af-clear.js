const $mongo = require('../libs/mongo');

const afDB = $mongo.collection('__af__queue');

console.log('start');

function handleError (err) {
  console.log(err);
}

afDB.deleteMany()
  .then(() => {
    console.log('done');
    process.exit();
  })
  .catch(handleError);